package fr.afpa.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import fr.afpa.beans.Adresse;
import fr.afpa.beans.Compte;
import fr.afpa.beans.User;

public class DaoUser {

	DbConn conn = new DbConn();

	private boolean verifAdresse = false;
	private String numRue;
	private String nomRue;
	private String codeP;
	private String ville;

	private String nom;
	private String prenom;
	private String mail;
	private String nomLib;
	private String tel;

	private String login;
	private String mdp;
	
	private int loginUserID;

	/**
	 * 
	 * @param numRue
	 * @param nomRue
	 * @param codeP
	 * @param ville
	 * @return
	 */
	public boolean ajoutAdresse(String numRue, String nomRue, String codeP, String ville) {

		Statement stmt = null;

		try {

			stmt = conn.connect().createStatement();

			String sql1 = "insert into adresse (nom_rue, num_rue, ville, code_postal) " + "values ('" + nomRue + "','"
					+ numRue + "', '" + ville + "', '" + codeP + "');";

			stmt.executeUpdate(sql1);

			stmt.close();

			conn.connect().close();

		} catch (Exception e) {
			System.err.println(e.getClass().getName() + " : " + e.getMessage());
			System.exit(0);
		}

		verifAdresse = true;
		return verifAdresse;
	}

	/**
	 * Fonction qui permet de cr�er un utilisateur
	 * 
	 * @param nom
	 * @param prenom
	 * @param mail
	 * @param tel
	 * @param nomLib
	 * @param numRue
	 * @param nomRue
	 * @param codeP
	 * @param ville
	 */
	public void ajoutUser(String nom, String prenom, String mail, String tel, String nomLib, String numRue,
			String nomRue, String codeP, String ville) {

		Statement stmt = null;

		try {

			stmt = conn.connect().createStatement();

			String sql2 = "insert into users (nom, prenom, mail, tel, nomLib, is_active, id_adresse) " + "values ('"
					+ nom + "', '" + prenom + "', '" + mail + "', '" + tel + "', '" + nomLib
					+ "' , true, (select id_adresse from adresse where nom_rue = '" + nomRue + "'and ville = '" + ville
					+ "'and code_postal = '" + codeP + "' and num_rue = '" + numRue + "'));";

			stmt.executeUpdate(sql2);

			String queryUser = "select id_user from users where nom = '" + nom + "' and prenom = '" + prenom
					+ "' and mail = '" + mail + "' and tel = '" + tel + "' and nomLib = '" + nomLib + "';";

			ResultSet resUser = stmt.executeQuery(queryUser);

			while (resUser.next()) {

				loginUserID = resUser.getInt("id_user");

			}	

			stmt.close();

			conn.connect().close();

		} catch (Exception e) {
			System.err.println(e.getClass().getName() + " : " + e.getMessage());
			System.exit(0);
		}

		System.out.println("Utilisateur bien ajout� !");
		
		System.out.println("---------- Voici votre compte ----------");
		System.out.println(recupCompte(loginUserID));

	}

	/**
	 * Method for authentification of a user
	 * 
	 * @param login
	 * @param mdp
	 * @return int : -1 if no valid, -2 if account is enable , id_user if it's valid
	 */
	public int auth(String login, String mdp) {

		PreparedStatement stmt = null;
		try {

			String query = "Select check_Login(?,?) ";

			stmt = conn.connect().prepareStatement(query);

			stmt.setString(1, login);
			stmt.setString(2, mdp);

			ResultSet res = stmt.executeQuery();

			if (res.next()) {

				return res.getInt(1);
			}

		} catch (SQLException e) {

			e.printStackTrace();

		} finally {

			if (stmt != null) {

				try {

					stmt.close();

				} catch (SQLException e) {

					e.printStackTrace();
				}
			}
		}

		return -1;

	}

	/**
	 * Method for search a user by id
	 * 
	 * @param idUser int : the id of the user
	 * @return User : the object from the bd
	 */
	public User findUserById(int idUser) {

		PreparedStatement stmt = null;

		User user = null;

		String query = "Select * from users where id_user = ? ";

		try {

			stmt = conn.connect().prepareStatement(query);

			stmt.setInt(1, idUser);

			ResultSet res = stmt.executeQuery();

			if (res.next()) {

				user = new User();
				user.setIdUser(idUser);
				user.setMail(res.getString(4));
				user.setNom(res.getString(2));
				user.setPrenom(res.getString(3));
				user.setNomLib(res.getString(6));
				user.setTel(res.getString(5));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {

					e.printStackTrace();
				}
			}
		}

		return user;
	}

	/**
	 * Fonction qui nous permet de modifier les infos personnelles d'un utilisateur
	 * 
	 * @param userID
	 * @param nom
	 * @param prenom
	 * @param mail
	 * @param tel
	 */
	public void updateUser(int userID, String nom, String prenom, String mail, String tel) {

		Statement stmt = null;
		try {

			stmt = conn.connect().createStatement();

			String sql1 = "update users set nom = '" + nom + "', " + "prenom = '" + prenom + "', " + "mail = '" + mail
					+ "', " + "tel = '" + tel + "'  " + "where id_user = " + userID + ";";
			stmt.executeUpdate(sql1);

			stmt.close();
			conn.connect().close();

		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}

		System.out.println("Vos infos ont bien �t� mis � jour !");

	}

	/**
	 * Fonction qui nous permet de d�sactiver un utilisateur ainsi que son compte
	 * 
	 * @param userID : on utilise l'id du user connecter
	 */
	public void isNotActive(int userID) {

		Statement stmt = null;
		try {

			stmt = conn.connect().createStatement();

			String sql1 = "update users set is_active = false where id_user = " + userID + ";";
			stmt.executeUpdate(sql1);

			String sql2 = "update compte set is_active = false where id_user = " + userID + ";";
			stmt.executeUpdate(sql2);

			stmt.close();
			conn.connect().close();

		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}

		System.out.println("Votre compte a bien �t� d�sactiver !");

	}

	/**
	 * Fonction qui nous permet de recuperer les infos de l'utilisateur connecter
	 * 
	 * @param userID : on utilise l'id du user connecter
	 * @return on retourne l'objet User
	 */
	public User consulterInfos(int userID) {

		Statement stmt = null;

		User user = null;
		Adresse adresse = null;

		try {

			stmt = conn.connect().createStatement();

			String queryUser = "select * from users where id_user = " + userID + ";";

			ResultSet resUser = stmt.executeQuery(queryUser);

			adresse = recupAdresse(userID);

			while (resUser.next()) {

				nom = resUser.getString("nom");
				prenom = resUser.getString("prenom");
				mail = resUser.getString("mail");
				tel = resUser.getString("tel");
				nomLib = resUser.getString("nomlib");

			}

			user = new User(userID, nom, prenom, mail, nomLib, tel, adresse);

			stmt.close();
			conn.connect().close();

		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		return user;
	}

	/**
	 * 
	 * @param userID : on utilise l'id du user connecter
	 * @return on retourne l'objet Adresse
	 */
	public Adresse recupAdresse(int userID) {
		Statement stmt = null;

		Adresse adresseU = null;

		try {

			stmt = conn.connect().createStatement();

			String queryAdresse = "select * from adresse where id_adresse = (select id_adresse from users where id_user = "
					+ userID + ");";

			ResultSet resAdr = stmt.executeQuery(queryAdresse);

			while (resAdr.next()) {

				numRue = resAdr.getString("num_rue");
				nomRue = resAdr.getString("nom_rue");
				codeP = resAdr.getString("code_postal");
				ville = resAdr.getString("ville");

			}

			adresseU = new Adresse(numRue, nomRue, codeP, ville);

			stmt.close();
			conn.connect().close();

		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		return adresseU;
	}

	/**
	 * Fonction qui permet de r�cuperer le login et le mdp de l'utilisateur qui
	 * viens d'�tre cr�er
	 * 
	 * @param userID : on utilise son id
	 * @return
	 */
	public Compte recupCompte(int userID) {

		Statement stmt = null;

		Compte compteU = null;

		try {

			stmt = conn.connect().createStatement();

			String queryAdresse = "select * from compte where id_user = " + userID + ";";

			ResultSet resCo = stmt.executeQuery(queryAdresse);

			while (resCo.next()) {

				login = resCo.getString("login");
				mdp = resCo.getString("mdp");

			}

			compteU = new Compte(login, mdp);

			stmt.close();
			conn.connect().close();

		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		return compteU;
	}
}
