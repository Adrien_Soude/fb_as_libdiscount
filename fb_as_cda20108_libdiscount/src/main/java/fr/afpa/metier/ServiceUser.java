package fr.afpa.metier;

import java.util.Scanner;

import fr.afpa.control.ControlSaisie;
import fr.afpa.dao.DaoUser;

public class ServiceUser {

	Scanner in = new Scanner(System.in);

	DaoUser daoU = new DaoUser();

	ControlSaisie controlS = new ControlSaisie();

	private String demandeNom;
	private String demandePrenom;
	private String demandeMail;
	private String demandeTel;
	private String demandeNomLib;
	private String demandeNumRue;
	private String demandeNomRue;
	private String demandeCodeP;
	private String demandeVille;

	public void demandeUser() {

		System.out.print("Quel est votre nom ? : ");
		demandeNom = in.nextLine();

		System.out.print("Quel est votre prenom ? : ");

		demandePrenom = in.nextLine();

		System.out.print("Quel est votre mail ? : ");
		demandeMail = in.nextLine();

		while (controlS.controlMail(demandeMail) != true) {

			System.out.print("Saisissez un mail contenant un '@' et un '.' ! : ");
			demandeMail = in.nextLine();

		}

		System.out.print("Quel est votre num�ro de t�l ? : ");
		demandeTel = in.nextLine();

		while (controlS.controlTel(demandeTel) != true) {

			System.out.print("Saisissez un t�l�phone contenant 10 chiffres ! : ");
			demandeTel = in.nextLine();

		}

		System.out.print("Quel est le nom de votre librairie ? : ");
		demandeNomLib = in.nextLine();

		System.out.print("Quel est le num�ro de rue de votre librairie ? : ");
		demandeNumRue = in.nextLine();

		System.out.print("Quel est le nom de rue de votre librairie ? : ");
		demandeNomRue = in.nextLine();

		System.out.print("Quel est le code postal de votre librairie ? : ");
		demandeCodeP = in.nextLine();

		while (controlS.controlCodeP(demandeCodeP) != true) {

			System.out.print("Saisissez un code postal contenant 5 chiffres ! : ");
			demandeCodeP = in.nextLine();

		}

		System.out.print("Quel est la ville de votre librairie ? : ");
		demandeVille = in.nextLine();
		demandeVille.toUpperCase();

		if (daoU.ajoutAdresse(demandeNumRue, demandeNomRue, demandeCodeP, demandeVille) == true) {
			daoU.ajoutUser(demandeNom, demandePrenom, demandeMail, demandeTel, demandeNomLib, demandeNumRue,
					demandeNomRue, demandeCodeP, demandeVille);
		}
	}

	public int auth(String login, String mdp) {
		return daoU.auth(login, mdp);
	}

	public void redirectionUpdateUser(int userID, String nom, String prenom, String mail, String tel) {

		daoU.updateUser(userID, nom, prenom, mail, tel);

	}

	public void desUser(int idUser) {

		daoU.isNotActive(idUser);

	}

	public void conInfos(int idUser) {
		
		daoU.recupAdresse(idUser);
		System.out.println(daoU.consulterInfos(idUser));

	}
}
