package fr.afpa.beans;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor

public class User {

	private int idUser;
	private String nom;
	private String prenom;
	private String mail;
	private String nomLib;
	private String tel;
	private Adresse adresse;

	@Override
	public String toString() {
		return "           votre ID  : " + idUser + "\n           nom  : " + nom + "\n           prenom : " + prenom
				+ "\n           mail : " + mail + "\n           librairie : " + nomLib + "\n           tel : " + tel;
	}
}
