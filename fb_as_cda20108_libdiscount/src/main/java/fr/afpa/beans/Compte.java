package fr.afpa.beans;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor

public class Compte {

	private String login;
	private String mdp;

	public String toString() {
		return "           Votre login  : " + login + "\n           nom  : " + mdp;
	}
}
