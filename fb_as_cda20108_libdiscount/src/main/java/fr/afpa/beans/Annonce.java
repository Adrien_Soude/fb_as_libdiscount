package fr.afpa.beans;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor


public class Annonce {

	
	private int idAnnonce;
	private String titre;
	private String isbn;
	private LocalDate dateEdition;
	private String maisonEdition;
	private double prixUni;
	private int quantite;
	private double remise;
	private LocalDate dateAnnonce;
	private int idUser;
	
	@Override
	public String toString() {
		return "\n#------------------------------- "+ titre +" -------------------------------#\n"
			  +"           Numero Annonce :"+ idAnnonce +"                                   \n"	
			  +"           Isbn :"+ isbn +"                                                  \n"	
			  +"           DateEdition :"+ dateEdition +"                                    \n"	
			  +"           Maison Edition : "+ maisonEdition +"                              \n"	
			  +"           Prix unitaire  : "+ prixUni +"                                    \n"	
			  +"           Quantite  : "+ quantite +"                                        \n"	
			  +"           Remise  : "+ remise +"                                            \n"
			  +"           Date de l'annonce : "+ dateAnnonce+"                              \n";
		
	} 
	
	
	

}
