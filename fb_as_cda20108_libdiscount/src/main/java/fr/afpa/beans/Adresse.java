package fr.afpa.beans;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor

/**
 * 
 * @author Adrien SOUDE && Fouad BOUCHLAGHEM
 *
 */
public class Adresse {

	private String numRue;
	private String nomRue;
	private String codeP;
	private String ville;

	public String toString() {
		return "\n           num�ro de rue  : " + numRue + "\n           nom de rue  : " + nomRue
				+ "\n           code postal : " + codeP + "\n           ville : " + ville;

	}
}
