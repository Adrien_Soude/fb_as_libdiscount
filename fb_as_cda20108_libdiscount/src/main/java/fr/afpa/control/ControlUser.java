package fr.afpa.control;

import fr.afpa.metier.ServiceUser;

public class ControlUser {

	ServiceUser su = new ServiceUser(); 
	
	public int auth(String login, String mdp) {
		return su.auth(login, mdp);
	}

	public void ajouterUser() {
		su.demandeUser();
	}

	public void demandeModif(int userID, String demandeNom, String demandePrenom, String demandeMail, String demandeTel) {
		
		su.redirectionUpdateUser(userID, demandeNom, demandePrenom, demandeMail, demandeTel);
		
	}

	public void disabledUser(int idUser) {

		su.desUser(idUser);
		
	}
	
	public void voirInfos(int idUser) {

		su.conInfos(idUser);
		
	}
	
}
